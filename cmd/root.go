package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// Run the root command
func Run() error {
	return rootCmd.Execute()
}

// Define the root command
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "Simple cobra command example",
	Run:   rootFn,
}

// Root function
func rootFn(cmd *cobra.Command, args []string) {
	fmt.Println("This is the root command")
}
