package cmd

// Define constants
const (
	App     = "cli"
	Version = "0.1"
)

// Define variables
var (
	Githash   = "HEAD"
	BuildDate = "1970-01-01T00:00:00Z UTC"
)
