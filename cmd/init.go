package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

// Initialize command
func init() {

	// Call initConfig function
	cobra.OnInitialize(initConfig)

	// Get cli flags
	pf := rootCmd.PersistentFlags()

	pf.StringP("config", "c", "config.yaml", "config file to use")

	// Detect cli errors
	err := viper.BindPFlags(pf)
	if err != nil {
		log.WithError(err).Fatal("fail to bind persistent flags")
	}
}

// Initialize config
func initConfig() {

	// Set viper defaults
	viper.SetDefault("global.log-level", 4)

	// Environment variables management
	viper.SetEnvPrefix(App)
	viper.AutomaticEnv()

	// Set config search path
	viper.AddConfigPath("/etc/" + App + "/")
	viper.AddConfigPath("$HOME/." + App)
	viper.AddConfigPath(".")

	// Define log format
	log.SetFormatter(
		&log.TextFormatter{
			FullTimestamp:   true,
			TimestampFormat: "2006-01-02 15:04:05.000000",
		},
	)

	// Load user defined config or use default one
	config := viper.GetString("config")
	if "" != config {
		viper.SetConfigFile(config)
	} else {
		viper.SetConfigName("config")
	}

	// Read config file
	if err := viper.ReadInConfig(); err != nil {
		//log.Fatalf("error config file: %v", err)
	}

	// Define log level
	level := viper.GetInt("global.log-level")
	log.SetLevel(log.InfoLevel)
	if level >= 0 && level < len(log.AllLevels) {
		log.SetLevel(log.AllLevels[level])
	}
}
