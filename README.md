# Gocli

A go cli example using cobra

## Requirements

* go 1.17.x

## How-to

### Build it

```bash
git clone https://gitlab.com/elnox/gocli.git
cd ./gocli && make
```

### Run it

```bash
./build/gocli help

Simple cobra command example

Usage:
  cli [flags]
  cli [command]

Available Commands:
  help        Help about any command
  version     Show version

Flags:
  -c, --config string   config file to use (default "config.yaml")
  -h, --help            help for cli

Use "cli [command] --help" for more information about a command.
```
