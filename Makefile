NAME				:= gocli
PROJECT				:= gitlab.com/elnox
BUILD_DIR			:= build
CC					:= GO111MODULE=on go build
DFLAGS				:= -race
CROSS				:= GOOS=linux GOARCH=amd64

VERSION				:= $(shell [ -d .git/ ] && git describe --tags --candidates 1)
GITHASH				:= $(shell [ -d .git/ ] && git rev-parse HEAD)
BUILDDATE			:= $(shell TZ=UTC date -u '+%Y-%m-%dT%H:%M:%SZ')
CFLAGS				:= -X ${PROJECT}/$(NAME)/core.Githash=$(GITHASH) -X ${PROJECT}/$(NAME)/core.Version=$(VERSION) -X ${PROJECT}/$(NAME)/core.BuildDate=$(BUILDDATE)

rwildcard			:= $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))
MODULES_PATHS		:= $(call rwildcard, ./cmd/..., *.go)
LINT_PATHS			:= $(NAME).go ./cmd/...
FORMAT_PATHS		:= $(NAME).go ./cmd
VPATH				:= $(BUILD_DIR)

.SECONDEXPANSION:

.PHONY: all
all: format release

.PHONY: init
init:
	[ -f go.mod ] && rm go.mod; go mod init ${PROJECT}/${NAME}

.PHONY: build
build: $(NAME).go $(MODULES_PATHS)
	$(CC) $(DFLAGS) -ldflags "$(CFLAGS)" -o $(BUILD_DIR)/$(NAME) $(NAME).go

.PHONY: release
release: $(NAME).go $(MODULES_PATHS)
	$(CC) -ldflags "$(CFLAGS)" -o $(BUILD_DIR)/$(NAME) $(NAME).go

.PHONY: format
format:
	gofmt -w -s $(FORMAT_PATHS) $(NAME).go

.PHONY: run
run:
	go run ${NAME}.go

.PHONY: vendor
vendor:
	go mod vendor

.PHONY: clean
clean:
	rm -rv build; rm -r vendor
