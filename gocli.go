package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/elnox/gocli/cmd"
)

func main() {
	if err := cmd.Run(); err != nil {
		log.WithError(err).Fatal("cannot run command")
	}
}
